<div class="local-wrapper">
    <div class="container">
        <div class="row">

            <div class="col-12 col-md-4 local-box">
                <h2>LONDON</h2>
                <h3>180 -182 REGENT STREET, LONDON, W1B 5BT</h3>
                <p>There are many variations of passages of Lorem Ipsum available, but the 
                    majority have suffered alteration in some form, by injected humour, or 
                    randomised words which don't look even. 
                </p>
                <div class="btn-local">
                  <button type="button" class="btn btn-outline-dark btn-sm local-button">VIEW DETAILS</button>
                </div>
            </div>

            <div class="col-12 col-md-4 local-box">
                <h2>NEW YORK</h2>
                <h3>109 COLUMBUS CIRCLE, NEW YORK, NY 10023</h3>
                <p>There are many variations of passages of Lorem Ipsum available, but the 
                    majority have suffered alteration in some form, by injected humour, or 
                    randomised words which don't look evene. 
                </p>
                <div class="btn-local">
                  <button type="button" class="btn btn-outline-dark btn-sm local-button">VIEV DETAILS</button>
                </div>
            </div>

            <div class="col-12 col-md-4 local-box">
                <h2>PARIS</h2>
                <h3>2133 RUE SAINT-HONORE, 75001 PARIS</h3>
                <p>There are many variations of passages of Lorem Ipsum available, but the 
                    majority have suffered alteration in some form, by injected humour, or 
                    randomised words which don't look believable. 
                </p>
                <div class="btn-local">
                  <button type="button" class="btn btn-outline-dark btn-sm local-button">VIEV DETAILS</button>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="map-wrapper">
   <div class="container-fluid">
   <div class="row">
           <div class="col-12 col-md-8 col-lg-9 local-maps">
               <div class="row">
                   <!-- <img class="img-fluid" src="images/avenue-fashion-stores-map.jpg" alt="avenie fashion mapa"> -->
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.0048193182024!2d-0.14277958400661098!3d51.51312757963606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604d502268421%3A0x6a7d62889992f993!2zUmVnZW50IFN0LCBMb25kb24sINCS0LXQu9C40LrQsCDQkdGA0LjRgtCw0L3QuNGY0LA!5e0!3m2!1ssr!2srs!4v1603110219186!5m2!1ssr!2srs" width="1400" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
           </div>
           <div class="col-12 col-md-4 col-lg-3">
               <div class="row">
                   <div class="col-12 local-text">
                        <h5>LONDON</h5>
                        <h6>180 -182 REGENT STREET,LONDON, W1B 5B8</h6>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have 
                            suffered alteration in some form, by injected humour, or randomised words which don't 
                            look even. </p>

                        <p><img class="img-fluid" src="images/avenue-map.svg" alt="avenie fashion mapa"> 180-182 Regent Street, London W1B 5BT</p>
                        <p><img class="img-fluid" src="images/avenue-telefon.svg" alt="avenie fashion mapa"> 0123-456-789</p>
                        <p><img class="img-fluid" src="images/link-solid.svg" alt="avenie fashion mapa"> www.yourwebsite.com</p>
                        <p><img class="img-fluid" src="images/avenue-envelope.svg" alt="avenie fashion mapa"> support@yourwebsite.com</p>
                        <p><img class="img-fluid" src="images/avenue-clock.svg" alt="avenie fashion mapa"> Monday-Friday:9am to 6pm Saturday: 10am to 6 pm Sunday:10am to 2pm</p>
                   </div>
                   <div class="col-12 local-media">
                   <img class="img-fluid" src="images/avenue-facebook.svg" alt="Avenue fashion Ave">
                      <img class="img-fluid" src="images/avenue-twitter.svg" alt="Avenue fashion Ave">
                      <img class="img-fluid" src="images/avenue-instagram.svg" alt="Avenue fashion Ave">
                      <img class="img-fluid" src="images/avenue-pinterest.svg" alt="Avenue fashion Ave">

                   </div>
               </div>
               
           </div>
        </div>

   </div>
</div>