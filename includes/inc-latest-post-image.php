<div class="the-brand-wrapper">
    <div class="container-fluid">
        <div class="row">
          <img class="img-fluid image-big" src="images/avenue-fashion-ave-brand-1.jpg" alt="Avenue fashion Ave">
          <img class="img-fluid image-small" src="images/avenue-fashion-ave-brand-2.jpg" alt="Avenue fashion Ave">

          <div class="brand-section">
            <h2><span>OUR</span> LOOKBOOK</h2>
            <h3>LATEST POSTS - MENS & WOMENS</h3>
          </div>
     </div>
</div>