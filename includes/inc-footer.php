<footer>

    <div class="footer-wrapper">
        
        <div class="container footer-links">
          <div class="row">
          
                    <div class="col-12 col-md footer-links-box">
                        <h5>INFORMATION</h5>
                        <a href="#">The brand</a>
                        <a href="#">Local stores</a>
                        <a href="#">Customer service</a>
                        <a href="#">Privacy & Cookies</a>
                        <a href="#">Site map</a>
                    </div>

                    <div class="col-12 col-md footer-links-box">
                        <h5>WHY BY FROM US</h5>
                        <a href="#">Shipping & returns</a>
                        <a href="#">Secure shopping</a>
                        <a href="#">Testimonials</a>
                        <a href="#">Awards winning</a>
                        <a href="#">Ethical trading</a>
                    </div>

                    <div class="col-12 col-md footer-links-box">
                         <h5>YOUR ACCOUNT</h5>
                        <a href="#">Sing in</a>
                        <a href="#">Register</a>
                        <a href="#">View cart</a>
                        <a href="#">View your lookbook</a>
                        <a href="#">Track order</a>
                        <a href="#">Update information</a>
                    </div>

                    <div class="col-12 col-md footer-links-box">
                        <h5>LOOKBOOK</h5>
                        <a href="#">Latest post</a>
                        <a href="#">Men's lookbook</a>
                        <a href="#">Woomen's lookbook</a>
                        <a href="#">Lookbooks RSS feed</a>
                        <a href="#">View your lookbook</a>
                        <a href="#">Delete your lookbook</a>
                    </div>

                    <div class="col-12 col-md footer-links-box">
                         <h5>CONTACT DETAILS</h5>
                         <p>Head Office: Avenue Fashion, 180-182 Regent Street, London.</p>
                        
                         
                         <p>Telephone: 0123-456-789</p>
                         
                         <p>Email: support@yourwebsite.com</p>
                    </div>

          </div>
        
        </div>
        <!--footer-media -->
        <div class="footer-media">
            <div class="container">
                <div class="row">

                   <div class="col-12 col-md naslov-fut">
                      <h6>AWARD WINNER</h6>
                      <p>FASHION AWARDS 2016</p>
                   </div>

                   <div class="col-12 col-md media-fut">

                      <img class="img-fluid" src="images/avenue-fashion-facebook.svg" alt="Avenue fashion Ave">
                      <img class="img-fluid" src="images/avenue-fashion-twitter.svg" alt="Avenue fashion Ave">
                      <img class="img-fluid" src="images/avenue-fashion-instagram.svg" alt="Avenue fashion Ave">
                      <img class="img-fluid" src="images/avenue-fashion-pinterest.svg" alt="Avenue fashion Ave">

                   </div>

                </div>
            
            </div>
        
        </div>
         <!--footer-media end-->
        <!--signature-->
        
        <div class="signature-wrapper">
            <div class="container">
                <div class="row">
                   <div class="col-12 col-md-6 signa1">
                      <p>&copy 2016 Avenue fashion</p>
                   </div>
                   <div class="col-12 col-md-6 signa2">
                       <p>Design by RobbyDesign.com</p>
                       <p>Dev by NikyMan</p>
                   </div>
                </div>
            </div>
        </div>

        <!--signature end-->
    
    </div>

</footer>