<div class="product-wrapper">
    <div class="container">
            <div class="row">

                <div class="col-12 col-md-6 product-box">
                                    
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img src="images/avenue-fashion-item-1.jpg" class="d-block w-100" alt="avelie fashion items">
                            </div>

                            <div class="carousel-item">
                            <img src="images/avenue-fashion-item-1.jpg" class="d-block w-100" alt="avelie fashion items">
                            </div>

                            <div class="carousel-item">
                            <img src="images/avenue-fashion-item-1.jpg" class="d-block w-100" alt="avelie fashion items">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon ave-control" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon ave-control" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        </div>

                </div>

                 <div class="col-12 col-md-6 product-box">
                        <div class="row">

                            <div class="col-12 hedline-box">
                                <h2>AVE CLASSIC SWETSHIRT</h2>
                            </div>
                            <div class="col-12 stars-box">
                                    <img class="starsi" src="images/star-solid.svg" alt="ave fashion star">
                                    <img src="images/star-solid.svg" alt="ave fashion star">
                                    <img src="images/star-solid.svg" alt="ave fashion star">
                                    <img src="images/star-solid.svg" alt="ave fashion star">
                                    <img src="images/star-solid1.svg" alt="ave fashion star">
                                    <p><a href="#">3 Review(s) |</a><a href="#"><span> Add a Review </span></a> | Share </p>
                                    <img class="big-face view-media" src="images/avenue-facebook.svg" alt="ave fashion media">
                                    <img class="view-media" src="images/avenue-twitter.svg" alt="ave fashion media">
                                    <img class="view-media" src="images/avenue-googleplus.svg" alt="ave fashion media">
                                    <img class="view-media" src="images/avenue-pinterest.svg" alt="ave fashion media">
                                    <img class="view-media" src="images/avenue-instagram.svg" alt="ave fashion media">
                                    <img class="view-media" src="images/avenue-envelope.svg" alt="ave fashion media">
                            </div>

                            <div class="col-12 text-des-box">

                                    <h3 class="price-view-small">£ </h3>
                                    <h3 class="price-no price-view"> 107</h3> 
                                    <h3 class="price-view-small-1">£</h3>
                                    <h3 class="price-view"> 89.99</h3>

                                    <p class="view-avilability" >AVAILABILITY : <span> In stock</span></p>
                                    <p class="view-code">PRODUCT CODE : <span>#499577</span></p>
                                    <p class="view-tag" >TAGS : <span>Classic, Casual, V-neck, Lose</span></p>

                                    <p class="view-justify">It is a long established fact that a reader will be distracted by the 
                                        readable content of a page when looking at its layout. The point of 
                                        using Lorem Ipsum is that it has</p>
                                    <ul class="view-steps">
                                        <li>Elasticated cuffs</li>
                                        <li>Casual fit</li>
                                        <li>100% Cotton</li>
                                        <li>Free shipping whit 4 days delivery</li>
                                    </ul>

                            </div>

                            <div class="size-wrapper">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">

                                             <form>
                                                  <div class="row">
                                                        <div class="form-group form-size col-12 col-md-5">
                                                            <label class="select-da" for="inputState">COLOR</label>
                                                            <select id="inputState" class="form-control select-bl">
                                                                <option class="select-bl" selected>Select Colour</option>
                                                                <option class="select-bl">Black</option>
                                                                <option class="select-bl">Red</option>
                                                                <option class="select-bl">Blue</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group form-size col-12 col-md-5">
                                                            <label class="select-da" for="inputState">SIZE</label>
                                                            <select id="inputState" class="form-control select-bl">
                                                                <option class="select-bl" selected>Select Size</option>
                                                                <option class="select-bl">42</option>
                                                                <option class="select-bl">44</option>
                                                                <option class="select-bl">46</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group form-size col-12 col-md-2">
                                                            <label class="mr-sm-2 select-da" for="inlineFormCustomSelect">QTU</label>
                                                            <select class="custom-select select-bl select-bl-da mr-sm-2" id="inlineFormCustomSelect">
                                                                <option class="select-bl" selected>1</option>
                                                                <option class="select-bl" value="1">2</option>
                                                                <option class="select-bl" value="2">3</option>
                                                                <option class="select-bl" value="3">4</option>
                                                            </select>
                                                        </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="buy-wrapper">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-6 buy1">
                                        <!-- <img src="images/view-cart.svg" alt=""> -->
                                             <button type="button" class="btn btn-outline-dark btn-lg view-button"><i class="fas fa-shopping-cart"></i> ADD TO CART</button>
                                        </div>

                                        <div class="col-12 col-md-6 buy1">
                                        <!-- <img src="images/view-heart.svg" alt=""> -->
                                          <button type="button" class="btn btn-outline-dark btn-lg view-button"><i class="far fa-heart"></i> ADD TO LOOK</button>
                                       
                                        </div>
                                        <div class="col-12 view-compare">
                                            <img src="images/view-arr.jpg" alt="avenue fashion arrow">
                                            <h5>ADD TO COMPARE</h5>
                                        </div>
                                    </div>
                                </div>
                           </div>

                            

                     </div>
                 </div>

         </div>
    </div>
</div>




<div class="view-specification-wrapper">
    <div class="container">
        <div class="specif-box">

             <div class="row">
                        <div class="col-6 col-md view-text">
                           <a href="#"><h2>DESCRIPTION</h2></a>  
                       </div>
                       <div class="col-6 col-md view-text">
                           <a  href="#"><h2> VIDEO </h2></a>
                       </div>
                       <div class="col-6 col-md view-text">
                           <a href="#"><h2>SIZE & SPECS</h2></a>
                       </div>
                    
                       <div class="col-6 col-md view-text">
                          <a href="#"><h2>REVIEWS</h2></a>
                       </div>
                       <div class="col-6 col-md view-text">
                           <a href="#"><h2>DELIVERY & RETURNS</h2></a>
                       </div>
             </div>
         </div>
    </div>
</div>

<div class="description-wrapper">
      <div class="container">
         <div class="row">
             <div class="col-12 des-text">
                    <h4>NUN EGESTAS POSUERE ENIM, EU MAXIMUS ERAT POSUERE EGET</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore 
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                    mollit anim id est laborum."
                    </p>

                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem 
                    aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo 
                    enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui 
                    atione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, 
                    adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. 
                    Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi 
                    consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, 
                    vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                    </p>
              </div>
         </div>
      </div>
</div>