<div class="look-wrapper">
    <div class="container-fluid lookbook-container">
     <div class="row">
       <div class="col-12 col-md-6 col-lg look-book">
         <div class="row">
            <img class="img-fluid" src="images/avenue-fashion-look-8.jpg" alt="Avenue fashion Ave">
               <div class="book-box">
                  <h4>MEN'S <br>
                     <span>LOOKBOOK</span>
                  </h4>
                  <p>Ipsum passages, and more recently with desktop publishing software like Aldus 
                  PageMaker including versions of. </p>
               
                  <div class="btn-look-box">
                     <button type="button" class="btn btn-outline-dark btn-lg btn-box">VIEW NOW</button>
                  </div>
               </div>
            </div>
       </div>

       <div class="col-12 col-md-6 col-lg look-book">
          <div class="row">
            <img class="img-fluid" src="images/avenue-fashion-look-9.jpg" alt="Avenue fashion Ave"> 
            <div class="book-box">
               <h4>WOMEN'S <br>
               <span>LOOKBOOK</span>
               </h4>
               <p>Ipsum passages, and more recently with desktop publishing software like Aldus 
               PageMaker including versions of.</p>
               <div class="btn-look-box">
                  <button type="button" class="btn btn-outline-dark btn-lg btn-box">VIEW NOW</button>
               </div>
            </div>
         </div>
      </div>

       <div class="col-12 col-md-6 col-lg look-book">
          <div class="row">
            <img class="img-fluid" src="images/avenue-fashion-look-10.jpg" alt="Avenue fashion Ave">
            <div class="book-box">
               <h4>YOUR'S <br>
               <span>LOOKBOOK</span>
               </h4>
               <p>Ipsum passages, and more recently with desktop publishing software like Aldus 
               PageMaker including versions of.</p>
               <div class="btn-look-box">
                  <button type="button" class="btn btn-outline-dark btn-lg btn-box">VIEW NOW</button>
               </div>
            </div>
         </div>
       </div>
     
     </div>
   </div>
</div>