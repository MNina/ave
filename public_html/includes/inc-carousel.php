
<div id="carIndicators" class="carousel slide ave-car" data-ride="carousel">
                        <!-- <ol class="carousel-indicators indicators-delovi d-none d-sm-block d-md-block">
                            <li data-target="#carIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carIndicators" data-slide-to="1"></li>
                            <li data-target="#carIndicators" data-slide-to="2"></li>
                        </ol>  -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <picture>
                                <source media="(max-width:500px)" srcset="images/ave-fashion-slider-02.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                <source media="(max-width:501px)" srcset="images/ave-fashion-slide2.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                  <img class="d-block w-100" src="images/ave-fashion-slide2.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                </picture>
                               
                            </div>
                            
                            <div class="carousel-item">
                            <picture>
                                <source media="(max-width:500px)" srcset="images/ave-fashion-slider-03.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                <source media="(max-width:501px)" srcset="images/slider/ave-fashion-slide3.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                <img class="d-block w-100" src="images/ave-fashion-slide3.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                           <picture>
                                
                            </div>

                            <div class="carousel-item">
                            <picture>
                                <source media="(max-width:500px)" srcset="images/ave-fashion-slider-04.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                <source media="(max-width:501px)" srcset="images/ave-fashion-slide4.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                                <img class="d-block w-100" src="images/ave-fashion-slide4.jpg" alt="Prodaja Ssangyong auto delova | Beograd, Srbija">
                            </picture>
                               
                            </div>

                        
                        </div>
                        <a class="carousel-control-prev" href="#carIndicators" role="button" data-slide="prev">
                            <div class="car-arrow-wrapper">
                               <span class="carousel-control-prev-icon carousel-arrow" aria-hidden="true"></span>
                            </div>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carIndicators" role="button" data-slide="next">
                          <div class="car-arrow-wrapper">
                              <span class="carousel-control-next-icon carousel-arrow" aria-hidden="true"></span>
                          </div>
                          <span class="sr-only">Next</span>
                        </a>
 
         </div>
                  