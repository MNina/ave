
<div class="over-nav">
    <div class="container">
        <div class="row">
             <div class="col-lg-8 col-md-6 col-3 currency">
                 <p>Currency :</p>
                 <div class="card-wrapper">
                           <div class="card-holder">
                           <div class="arrow-currency">
                            <img class="image-fluid" src="images/angle-down-ava.svg" alt="">
                        </div>
                            <div class="card-input"></div>
                            <ul class="card-list">
                                <li>GBP</li>
                                <li>EUR</li>
                                <li>USD</li>
                            </ul>
                        </div>
                 </div>
                
                 
             </div>
             <div class="col-lg-4 col-md-6 col-9 over-nav-2">
                    <div class="row">
                        <div class="col-md-3  over-1 over-ali d-none d-sm-block">
                           <a href="sing-in.php">Register</a>
                        </div>
                        <div class="col-md-4 col-4 over-1">
                           <a href="sing-in.php">Sing In</a>
                        </div>

                        <div class="col-md-5 col-8 over-2">  
                            <img class="img-fluid" src="images/chart-nav.jpg" alt="Chart">
                            <p>empty</p><img class="img-fluid image-arr" src="images/chart-angle.svg" alt="Chart">

                            
                            
                        </div>
                    </div>
             </div>
        
        </div>
    </div>
</div>

    <div class="nav-wrapper" id="deloviStickyNav">
       
            
        
                <nav class="navbar navbar-expand-lg navbar-light bg-light ave-nav">
                    <a class="ave-logo navbar-brand" href="index.php"><img class="img-fluid" src="images/avenue-fashion-logo.jpg" alt="Avenue Fashion Logo"></a>
                    <button class="navbar-toggler ave-nav-btn" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                         <div class="collapse navbar-collapse nav-trans" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto ave-ul">

                                        <li class="nav-item brand-text">
                                            <a  href="index.php"><h2>HOME<h2></a>
                                        </li>

                                        <li class="nav-item dropdown drop1">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <h2>MENS</h2>
                                            </a>
                                            <div class="dropdown-menu dropdown-avenue drop-open" aria-labelledby="navbarDropdown">
                                                <div class="row">
                                                    <div class="col-md-6 col-12 pod-box">
                                                        <p>CASUAL</p> 
                                                        
                                                        <a class="dropdown-item" href="man-jacket.php">Jackets</a>
                                                        <a class="dropdown-item" href="#">Hoodies & Swetshirts</a>
                                                        <a class="dropdown-item" href="mans-shirt.php">Polo Shirts</a>
                                                        <a class="dropdown-item" href="#">Sportswear</a>
                                                        <a class="dropdown-item" href="#">Trousers & Chinos</a>
                                                        <a class="dropdown-item" href="mans-shirt.php">T-shirts</a>
                                                    </div>
                                                    <div class="col-md-6 col-12 podmeni-box pod-box">
                                                        <p>FORMAL</p> 
                                                        
                                                        <a class="dropdown-item" href="#">Jackets</a>
                                                        <a class="dropdown-item" href="mans-shirt.php">Shirts</a>
                                                        <a class="dropdown-item" href="mans-shirt.php">Polo Shirts</a>
                                                        <a class="dropdown-item" href="#">Suits</a>
                                                        <a class="dropdown-item" href="#">Trousers</a>
                                                    </div>
                                                    <div class="col image-poddomen">
                                                    <img class="img-fluid" src="images/avenue-fashion-sale.jpg" alt="avenue fashion sale">
                                                </div>
                                            </div>
                                            </div>
                                        </li>

                                        <li class="nav-item dropdown drop1">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <h2>WOMENS </h2>
                                            </a>
                                            <div class="dropdown-menu dropdown-avenue drop-open" aria-labelledby="navbarDropdown">
                                                <div class="row">
                                                    <div class="col-md-6 col-12 pod-box">
                                                        <p>CASUAL</p> 
                                                        
                                                        <a class="dropdown-item" href="woman-jacket.php">Jackets</a>
                                                        <a class="dropdown-item" href="#">Hoodies & Swetshirts</a>
                                                        <a class="dropdown-item" href="#">Polo Shirts</a>
                                                        <a class="dropdown-item" href="#">Sportswear</a>
                                                        <a class="dropdown-item" href="#">Trousers & Chinos</a>
                                                        <a class="dropdown-item" href="#">T-shirts</a>
                                                    </div>
                                                    <div class="col-md-6 col-12 podmeni-box pod-box">
                                                        <p>FORMAL</p> 
                                                        
                                                        <a class="dropdown-item" href="#">Jackets</a>
                                                        <a class="dropdown-item" href="#">Shirts</a>
                                                        <a class="dropdown-item" href="#">Polo Shirts</a>
                                                        <a class="dropdown-item" href="#">Suits</a>
                                                        <a class="dropdown-item" href="#">Trousers</a>
                                                    </div>
                                                    <div class="col image-poddomen">
                                                    <img class="img-fluid" src="images/avenue-fashion-sale.jpg" alt="avenue fashion sale">
                                                </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="nav-item brand-text">
                                            <a  href="the-brand.php"><h2>THE BRAND<h2></a>
                                        </li>

                                        <li class="nav-item brand-text">
                                            <a  href="local-stores.php"><h2>LOCAL STORES<h2></a>
                                        </li>

                                        <!-- <li class="nav-item dropdown drop1">
                                            <a class="nav-link dropdown-toggle" href="local-stores.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <h2>LOCAL STORES</h2>
                                            </a>
                                            <div class="dropdown-menu drop-open dropdown-avenue pod-box" aria-labelledby="navbarDropdown">
                                                        <p>EUROPE</p> 
                                                        
                                                        <a class="dropdown-item" href="#">London</a>
                                                        <a class="dropdown-item" href="#">New York</a>
                                                        <a class="dropdown-item" href="#">Paris</a>
                                            </div>
                                        </li> -->

                                        <li class="nav-item dropdown drop1">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <h2> LOOK BOOK </h2>
                                                </a>
                                            <div class="dropdown-menu  dropdown-avenue drop-open" aria-labelledby="navbarDropdown">
                                                <div class="row">
                                                    <div class="col-md-6 col-12 pod-box pod-box-look">
                                                        <p>OUR LOOKBOOKS</p> 
                                                        
                                                        <a class="dropdown-item" href="latest-post.php">Latest post (mixed)</a>
                                                        <a class="dropdown-item" href="#">Man's Lookbook</a>
                                                        <a class="dropdown-item" href="#">Women's lookbook</a>
                                                    </div>

                                                    <div class="col-md-6 col-12 podmeni-box pod-box">
                                                        <p class="mt-2">YOUR LOOKBOOK</p> 
                                                        
                                                        <a class="dropdown-item" href="#">View and Edit</a>
                                                        <a class="dropdown-item" href="#">Share</a>
                                                        <a class="dropdown-item" href="#">Delite</a>
                                
                                                    </div>
                                                    <div class="col image-poddomen">
                                                    <img class="img-fluid" src="images/avenue-fashion-sale.jpg" alt="avenue fashion sale">
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                    
                                </ul>

                                <div class="input-group ave-search">
                                    <input class="form-control form-control-search" type="search" value="Search..." id="example-search-input">
                                    <span class="input-group-append">
                                        <div class="input-group-text input-group-text-search bg-transparent"><i class="fa fa-search"></i></div>
                                    </span>
                                </div>
                        </div>
                </nav>
         
    </div>
</div>



    <script>
        //za stiky nav
        document.addEventListener('scroll', myFunction);
        var navbar = document.getElementById("deloviStickyNav");
        var sticky = navbar.offsetTop;
        function myFunction() {
            if (window.pageYOffset >= sticky) {
                navbar.classList.add("deloviSticky")
            } else {
                navbar.classList.remove("deloviSticky");
            }
        }
         
//za valute

$(function (){
    $(".card-wrapper .card-input").html($(".card-list li:first-child").html());

    $(".card-holder").click(function(){
        $(".card-list").toggleClass("visible")
    });
    $(".card-list li").click(function(){
        $(".card-input").html($(this).html());
    })
});




    </script> 
