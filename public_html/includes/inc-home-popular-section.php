<div class="popular-wrapper">
      <div class="container-fluid popular-container">
        <div class="row">

                <div class="col-12 popular-hedlines">
                   <div class="row">
                        <div class="col-6 col-md">
                            <button filter="filter1" type="button" class="btn  btn-sm ave-home-button filter-button active">POPULAR</button>
                       </div>
                       <div class="col-6 col-md">
                             <button filter="filter2" type="button" class="btn  btn-sm ave-home-button filter-button">NEW ARRIVALS</button>
                       </div>
                       <div class="col-6 col-md">
                             <button filter="filter3" type="button" class="btn  btn-sm ave-home-button filter-button">BEST SELLERS</button>
                       </div>
                       <div class="col-6 col-md">
                             <button filter="filter4" type="button" class="btn  btn-sm ave-home-button filter-button">SPECIAL OFFERS</button>
                       </div>
                       <div class="col-6 col-md">
                           <button filter="filter5" type="button" class="btn  btn-sm ave-home-button filter-button">COMMING SOON</button>
                       </div>
                    </div>
                </div>

               <div class="col-12 popular1">
                   <div class="row">

                        <div class="col-md-6 col-12 deo1">
                           <div class="row">
                             <div class="col-12 col-md-6 shopping-box filter1 filter2">
                                <img class="img-fluid" src="images/avenue-fashion-home-2.jpg" alt="Ave Avenue Fashion popular">
                                 <p class="img-price1">£ </p>
                                 <p class="img-price">89.99</p>
                                 <div class="shopping-part">
                                     <h3>MANS BURNT  CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>
                             <div class="col-12 col-md-6  shopping-box filter1 filter2">
                                <img class="img-fluid" src="images/avenue-fashion-home-3.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">47.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>
                             <div class="col-12  shopping-box filter1 filter3">
                                <img class="img-fluid" src="images/avenue-fashion-home-4.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">69.95</p>
                                <div class="shopping-part shopping-part2">
                                     <h3>MANS BURNT CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>
                           </div>
                        </div>

                       
                        <div class="col-md-6 col-12 deo2">
                           <div class="row">
                                <div class="col-12  shopping-box filter1 filter3">
                                        <img class="img-fluid" src="images/avenue-fashion-home-5.jpg" alt="Ave Avenue Fashion popular">
                                        <p class="img-price1">£ </p>
                                        <p class="img-price discount">107</p> 
                                        <p class="img-price2">£ </p>
                                        <p class="img-price-discount">89.99</p>
                                        <div class="shopping-part shopping-part2 filter1">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                                    </div>

                                    <div class="col-12 col-md-6  shopping-box filter1 filter2">
                                        <img class="img-fluid" src="images/avenue-fashion-home-6.jpg" alt="Ave Avenue Fashion popular">
                                        <p class="img-price1">£ </p>
                                        <p class="img-price">29.95</p>
                                        <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                                    </div>

                                    <div class="col-12 col-md-6  shopping-box filter1 filter2">
                                        <img class="img-fluid" src="images/avenue-fashion-home-7.jpg" alt="Ave Avenue Fashion popular">
                                        <p class="img-price1">£ </p>
                                        <p class="img-price" >34.79</p>
                                        <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                                    </div>
                           </div>
                        </div>


                        <div class="col-md-6 col-12 deo3">
                           <div class="row">
                             <div class="col-12 col-md-6 shopping-box filter5 filter2">
                                <img class="img-fluid" src="images/ave-fashion-home-01.jpg" alt="Ave Avenue Fashion popular">
                                 <p class="img-price1">£ </p>
                                 <p class="img-price">90.99</p>
                                 <div class="shopping-part">
                                     <h3>MANS BURNT  CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>
                             <div class="col-12 col-md-6  shopping-box filter5 filter2">
                                <img class="img-fluid" src="images/ave-fashion-home-02.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">42.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>

                             <div class="col-12 col-md-6  shopping-box  filter4">
                                <img class="img-fluid" src="images/ave-fashion-home-03.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">67.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>

                             <div class="col-12 col-md-6  shopping-box  filter4">
                                <img class="img-fluid" src="images/ave-fashion-home-04.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">57.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>

                           </div>
                        </div>
                   

                        <div class="col-md-6 col-12 deo4">
                           <div class="row">
                             <div class="col-12 col-md-6 shopping-box filter2">
                                <img class="img-fluid" src="images/ave-fashion-home-05.jpg" alt="Ave Avenue Fashion popular">
                                 <p class="img-price1">£ </p>
                                 <p class="img-price">89.99</p>
                                 <div class="shopping-part">
                                     <h3>MANS BURNT  CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women on the move.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>
                             <div class="col-12 col-md-6  shopping-box filter2">
                                <img class="img-fluid" src="images/ave-fashion-home-06.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">35.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>

                             <div class="col-12 col-md-6  shopping-box filter4">
                                <img class="img-fluid" src="images/ave-fashion-home-07.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">97.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>

                             <div class="col-12 col-md-6  shopping-box filter4">
                                <img class="img-fluid" src="images/ave-fashion-home-08.jpg" alt="Ave Avenue Fashion popular">
                                <p class="img-price1">£ </p>
                                <p class="img-price">122.50</p>
                                <div class="shopping-part">
                                     <h3>WOMENS BURNT ORANGE CESUAL TEE '29.95</h3>
                                      <p>Classic casual t-shirt for women.</p>
                                      <p>100% cotton</p>
                                      <img class="img-fluid" src="images/home-chart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-hart" alt="Ave Avenue Fashion popular">
                                      <img class="img-fluid" src="images/home-arrows" alt="Ave Avenue Fashion popular">
                                 </div>
                             </div>

                           </div>
                        </div>

                     


                   </div>
               </div>

        </div>

   </div>

</div>

<script>

   $(document).ready(function() {
      $( ".shopping-box" ).hover(function() {
         $( this ).find('.shopping-part').show();
      },
      function() {
         $( this ).find('.shopping-part').hide();
      }
      );
   });

   //klasiranje slika
        $( document ).ready(function() {
            let previusFilter = null;
            $( ".filter-button" ).click(function(e) {
                e.preventDefault();
                
                // Remove "active" class from all fillter buttons.
                $(".filter-button").removeClass("active");

                // Add "active class" to this fillter button.
                $(this).addClass("active");

                // Hide all products.
                $(".shopping-box").hide();

                // Show product with selected filter.
                const filter = $(this).attr("filter");
                $("." + filter).show();

                // If same filter is clicked again reset filter and shoq all products.
                if (previusFilter === filter) {
                    $(".filter-button").removeClass("active");
                    $(".shopping-box").show();
                    previusFilter = false;
                } else {
                    previusFilter = filter;
                }
            });
        });
 

</script> 