<!DOCTYPE html  lang="en">
    <html>
        <head>

            <!-- Meta properties -->
            <meta charset="utf-8">
            <meta name="robots" content="index, follow"/>
            <meta name="keywords" content="fashion avenue fashion"/>
            <meta name="description" content="Sale of branded clothing."/>
            <meta name="distribution" content="global"/>
            <meta name="author" content="Studio 77"/>
            <meta name="copyright" content="Web Dizajn Beograd | Studio 77"/>
            <meta name="reply-to" content="office@webdizajn-beograd.com"/>
            <meta name="googlebot" content="noodp"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

             <!-- Styles -->
             <title>AVE AVENUE FASHION</title>
             
             <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;600&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
            <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <link rel="stylesheet" type="text/css" src="./bootstrap/css/bootstrap.min.css">
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link href="/ave-home.css" rel="stylesheet" type="text/css">
            <link href="/stores.css" rel="stylesheet" type="text/css">
    
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        </head>

        <body>


       
            <?php 
                include('includes/inc-header.php');
            ?>


            <?php 
                include('includes/inc-the-local-image.php');
            ?>
    
           <?php 
                include('includes/inc-local.php');
            ?>
        

            <?php 
                include('includes/inc-footer.php');
            ?>






     
  </body>


    </html>